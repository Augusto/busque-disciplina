public class Disciplina{
	private String nome;
	private String professor;
	private int creditos;
	private int codigoDisciplina;
	private String preRequisitos;
	private String turma;
	private String turno;
	private String diaHorario;

	public void Disciplina(String nome, String codigoDisciplina, String diaHorario, String professor){
		this.setNome(nome);
		this.setCodigoDisciplina(codigoDisciplina);
		this.setDiaHorario(diaHorario);
		this.setProfessor(professor);	
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}

	public String getNome(){
		return this.nome;
	}
	
	public void setProfessor(String professor){
		this.professor = professor;
	}

	public String getProfessor(){
		return this.professor;
	}

	public void setCreditos(int creditos){
		this.creditos = creditos;
	}

	public int getCreditos(){
		return this.creditos;
	}	

	public void setCodigoDisciplina(int codigoDisciplina){
		this.codigoDisciplina = codigoDisciplina;
	}

	public int getCodigoDisciplina(){
		return this.codigoDisciplina;
	}

	public void setPreRequisitos(String preRequisitos){
		this.preRequisitos = preRequisitos;
	}	
	
	public String getPreRequisitos(){
		return preRequisitos;
	}	
	
	public void setTurma(String turma){
		this.turma = turma;
	}

	public String getTurma(){
		return this.turma;
	}	

	public void setTurno(String turno){
		this.turno = turno;
	}	 

	public String getTurno(){
		return this.turno;
	}

	public void setDiaHorario(String diaHorario){
		this.diaHorario = diaHorario;
	}	

	public String getDiaHorario(){
		return diaHorario;
	}
}	
